//Name: Xavier Wells
//ID#: 1001519607
package submit.giftwrap;

import java.util.Scanner;

public class SubmitGiftWrap {

    public static void main(String[] args) {
     
        Scanner in=new Scanner(System.in);
     
        int selection, price, length, width, height, surfaceArea, payment;
        
        System.out.printf("***Hello! Please pick from the following options:***%n"
                + "1) Buy standard wrapping paper-press 1%n"
                + "2) Buy holiday edition wrapping paper-press 2%n");
        
        selection=in.nextInt();
        
        if (selection==1)
        {
            System.out.printf("You have selected standard wrapping paper.");
            price=1;
        }
        else if (selection==2)
        {
            System.out.printf("You have selected holiday edition wrapping paper%n");
            price=2;
        }    
        else
        return;
      
        System.out.printf("Please enter the size of your gift (in inches):%n");
        
        System.out.printf("Length: ");
        length=in.nextInt();
        System.out.printf("Height: ");
        height=in.nextInt();
        System.out.printf("Width: ");
        width=in.nextInt();
        
        if (!(height==0 || width==0 || length==0)) //these dimensions are impossible for a box
        {} 
        else
        {
        //SA=2(wl+hl+hw)
        surfaceArea=2*((width*length)+(height*length)+(height*width));
        payment=price*surfaceArea;
        
        System.out.printf("You will pay $%d at the counter. Thank you for shopping with us!%n", payment);
        }    
        
    }
    
}
