//Name: Xavier Wells
//ID#: 1001519607
package submit.numberlength;

import java.util.Scanner;

public class SubmitNumberLength {

    public static void main(String[] args) {
       Scanner in=new Scanner(System.in);
       
       System.out.printf("Please enter a number betwen 4-6: ");
       
       int num=in.nextInt();  //user input for number 4-6
       
       String word;  
       String wordSubstring;
       if (num>6 || num<4)
        {    
            System.out.println("Number out of Range!");
        }
       
       else
        {
            System.out.printf("Enter a word with at least %d letters and less than 10 letters: ", num);
            word=in.next();
            
            
            int wordlength=word.length();
            
            if(wordlength<num)
            {
                System.out.println("Too few letters!");
            }
            else if(wordlength>10)
            {
                System.out.println("Too many letters");
            }    
            else if(wordlength>num || wordlength<10)
            {
            System.out.printf("Please enter another number between 1 and 3: ");
            int num2=in.nextInt();
                
                 if (num2<1 || num2>3)
                 {
                   System.out.println("number out of range");
                 }
                 else
                 {    
                 wordSubstring=word.substring(0,num2);
                 System.out.printf("%s %n", wordSubstring);
                 }
            }
        }
    }
}
