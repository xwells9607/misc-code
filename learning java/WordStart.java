//Name: Xavier Wells
//ID#: 1001519607
package wordstart;

import java.util.Scanner;

public class WordStart {

    public static void main(String[] args) {
     Scanner in=new Scanner(System.in);
     
     String word;
     
     System.out.printf("Please enter a word: ");
     
     word=in.next();  //user input
     
     String lowWord=word.substring(0, 1).toLowerCase();  //to compare to alphabet
     
     if (lowWord.substring(0, 1).matches("a||b||c||d||e||f||g||h||i||j||k||l||m||n||o||p||q||r||s||t||u||v||w||x||y||z"))
     {
      
         if (lowWord.substring(0, 1).matches("a||e||i||o||u")) //when y is the first letter of a sylable it is a consonant
         {
         System.out.println(""+word+" starts with a vowel.");
         }
         
         else
         {
         System.out.println(""+word+" starts with a consonant.");
         }
         
     }
     else
     {
         System.out.println(""+word+" starts with neither a vowel nor a consonant.");
     }
    
}
}