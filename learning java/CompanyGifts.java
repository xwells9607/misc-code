/*
 * CSE 1310-001
 * Xavier Wells
 * ID: 1001519607
 */

import java.util.Scanner;

public class CompanyGifts 
{
    public static void main(String[] args) 
  {
        
    Scanner in=new Scanner(System.in);
    
    String who;                             //manager, customer, exit, else repeat
    String nameBirth;               		//first last birthday(MM/DD/YYYY) seperated by spaces
    String[] custInfo=new String[7];        //array to store customers information in the correct format
    String password="ABC 132";              //the managers password, hvaing it a seperate string allows for more flexibility
    String manChoice;                       //print to screen or exit
    int k=0;                                //loop to make customer info only printout as much as entered
    int j=0;                                //created for loop of customer input, allowing only 7 customers
    int i=0;                                //created for loop if anything other than password, customer or exit is entered 
    boolean customer=false;
    while(i<1&&i>-1)                        //determines if customer/manager, exit or repeat
    {
        System.out.printf("***\nEnter the word \"customer\" if you are a customer\nor your ID if you are the manager\n");
        who=in.nextLine();
        
            if (who.matches(password)|who.toLowerCase().matches("customer")) 
            {
            i++;
            
                if (who.matches(password))  //commands to follow if password is input
                {
                    
                    while (i==1)
                    {
                    System.out.printf("***\nHello manager. What would you like to do?\nPrint to screen or exit?\n***\n");
                    manChoice=in.nextLine(); 
                    
                                if (manChoice.toLowerCase().matches("exit"))
                                {
                                i--; //back to first loop
                                }
                                
                                else if (manChoice.toLowerCase().matches("print to screen"))
                                {
                                i++;
                                    
                                    if (customer)
                                    {
                                    System.out.printf("***\nCUSTOMERS\n\n");
                                    
                                        while (k<j)
                                        {
                                        System.out.printf("%s\n\n",custInfo[k]);
                                        k++;
                                        }
                                        k=0;
                                        
                                    i--;
                                    }
                                    
                                    else
                                    {
                                    System.out.println("***/nSorry, no customers have entered any information yet");
                                    i--;
                                    }
                                }
                            else 
                                {
                                i=1; //continue this loop
                                }
                    }
                }
                else                //comands to follow if customer is entered
                {
                    
                    if (j<7)
                    {
                    System.out.printf("***\nHello customer. Please enter your name (first and last)\nfollowed by your birthday (MM/DD/YYYY:)\n***\n");
                    nameBirth=in.nextLine();   
                    String[] splitName = nameBirth.split("\\s");    //create array that splits the input
                    custInfo[j]=("BIRTHDAY: "+splitName[2]+"       NAME: "+splitName[1]+", "+splitName[0]);    //insert previous array into main array for formatting and easy storage
                    System.out.println("Thank you "+splitName[0]+"!");
                    
                    j++;
                    i=0;
                    customer=true;
                    }
                    
                    else                    //only 7 customers allowed
                    {
                    System.out.println("Sorry, no more customers.");
                    i=0;
                    }
                }
                    
            } 
            
            else if (who.toLowerCase().matches("exit"))
            {
            System.out.println("Bye!\n***");
            i--;
            }
            
            else
            {
            i=0;   //loopback
            }
        
    }

  }
    
}
