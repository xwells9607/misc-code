//Name: Xavier Wells
//ID#: 1001519607
package monthstonumbers;

import java.util.Scanner;

public class MonthsToNumbers {

    public static void main(String[] args) {
       Scanner in=new Scanner(System.in);
       String m1="january";
       String m2="february";
       String m3="march";
       String m4="april";
       String m5="may";
       String m6="june";
       String m7="july";
       String m8="august";
       String m9="september";
       String m10="october";
       String m11="november";
       String m12="december";
       
       System.out.printf("Please enter the name of a month: ");
       
       String userMonth=in.next();  //user input for month
       String compare=userMonth.toLowerCase();  //change all input to lowercase for comparison
       String capital = compare.substring(0, 1).toUpperCase() + compare.substring(1); //format month for output
       String number;  //create string to assign month number


       

                    if (compare.matches(m1))
                      {
               number="first";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                    else if (compare.matches(m2))
                      {
               number="second";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m3))
                      {
               number="third";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m4))
                      {
               number="fourth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m5))
                      {
               number="fifth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m6))
                      {
               number="sixth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m7))
                      {
               number="seventh";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m8))
                      {
               number="eighth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m9))
                      {
               number="ninth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m10))
                      {
               number="tenth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m11))
                      {
               number="eleventh";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else if (compare.matches(m12))
                      {             
               number="twelth";
               System.out.printf("%s is the %s month%n", capital, number);
                      }
                      else
                      {
               System.out.printf("Unknown month: %s%n", userMonth);
                      }

       
       }
}