// Xavier_Wells 1001519607
/* Coding Assignment 5 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "MyLib.h"

void ConvertDecimalToBinary(char OutputArray[])
{
	int DecNum;
	int AskAgain = 1;
	int BitArray[7];
	while (AskAgain)
	{
		printf("Please enter a decimal number between 0 and 255 ");
		scanf("%d", &DecNum);
	
		if (DecNum >= 0 && DecNum <= 255)
			AskAgain = 0;
		else
		{
			AskAgain = 1;
			printf("\nYou entered a number not between 0 and 255\n\n");
		}
	}

	int i;

	BitArray[7] = DecNum;
	
	/* right bitshift to divide by 2 */
	for (i = 6; i >= 0; i--)
	{
		BitArray[i] = BitArray[i+1] >> 1;
	}

	printf("\nDecimal %d converts to binary ", DecNum);
	
	/* use a bitmask of 1 to determine if odd or even */
	for (i = 0; i <= 7; i++)
	{
		BitArray[i] = (BitArray[i] & 1) ? 1 : 0;
		OutputArray[i]=(BitArray[i]+48);
	}
	
	return;
}

void SentenceCleanup(char OutputArray[])
{
	char input[500];
	char inputCopy[500];
	char delimiters[]=".!?\n";
	char *tokenptr;
	char punctuation;
	char *pptr;
	char *nospaceptr;
	char sentence[500];
	char sentenceReset[500];
	int i;

	getc(stdin);
	printf("Enter at least two sentences separated by one of these delimiters - .!? -\n(max of 500 characters total)\n\n");
	fgets(input, 500, stdin);
	
	input[strlen(input)-1]='\0';
	
	strcpy(inputCopy, input);

	tokenptr=strtok(input, delimiters);
	
	while(tokenptr!=NULL)
	{
		//grab the punctuation from the input copy
		punctuation=inputCopy[strlen(tokenptr)];
		pptr=inputCopy+strlen(tokenptr)+1;
		
		//delete the previous sentence
		strcpy(inputCopy,pptr);
		
		//throw out the spaces by using another ptr to point to AFTER the spaces
		//keep previous pointer unchanged for strtok loop purposes
		nospaceptr=tokenptr;
		do
		{
			if(*nospaceptr==' ')
			{
				++nospaceptr;
			}
		}
		while(*nospaceptr==' ');
		
		//capitalize first letter of each sentence
		nospaceptr[0]=toupper(nospaceptr[0]);
		
		//clear sentence for next iteration, otherwise we get junk. memset might work here
		for(i=0;i<sizeof(sentence);i++)
		{
			sentence[i]=0;
		}
		
		//create final output with punctuation
		strcpy(sentence,nospaceptr); //copy token to string
		sentence[strlen(sentence)]=punctuation; //add the punctuation
		sentence[strlen(sentence)]=' '; //add a space to the end of the sentence
		strcat(OutputArray,sentence);
		

		
		//continue finding tokens
		tokenptr=strtok(NULL, delimiters);

	}
	
	return;
}

void SKUConverter(char OutputArray[])
{
	char input[50];
	int i;
	int j=0;
	char LeftSide[7];
	char *dashptr;
	char RightSide[4];
	int RightLength;
	char FinalSKU[8];
	
	//get user input and make length shortcut
	printf("Enter a dashed SKU\n");
	getc(stdin);
	fgets(input,sizeof(input),stdin);
	
	//remove trailing new line
	strtok(input,"\n");
	
	//check length
	if (strlen(input)>8)
	{
		printf("The entered dashed SKU is not valid");
		return;
	}
	
	//check dash position
	else if (isdigit(input[0])==0)
	{
		printf("Dash must be between two numbers");
		return;
	}
	else if (isdigit(input[strlen(input)-1])==0)
	{
		printf("Dash must be between two numbers");
		return;
	}
	
	//check amount of numbers and dash count
	else 
	{
		for (i=0;i<strlen(input);i++)
		{
			//check for dash(es)
			if (input[i]=='-')
			{
				j++;
			}
			
			//check for illegal digits
			if (strchr("-0123456789",input[i])==NULL)
			{
				printf("Input SKU can only contain numbers and one dash");
				return;
			}
			
		}
		
		//check for undashed SKU
		if (j==0&&i==7)
		{
			strcpy(OutputArray, input);
			return;
		}
		
		//check dash exists
		else if(j==0)
		{
			printf("Input SKU must contain a dash");
			return;
		}
			
		//only one dash is allowed
		else if (j>1)
		{
			printf("Input SKU can only contain one dash");
			return;
		}
		
		//put all input in LeftSide
		strcpy(LeftSide,input);
		
		//Seperate Left and Right with /0
		dashptr=strtok(LeftSide,"-");
		//move right to RightSide
		strcpy(RightSide,dashptr+strlen(LeftSide)+1);
		
		//pad left on right with zeros if necessary
		//clear out any old values passed besides what's needed in  left side
		for(i=(sizeof(LeftSide)-(sizeof(LeftSide)-strlen(LeftSide)));i<=sizeof(LeftSide);i++)
		{
			LeftSide[i]=0;
		}
		//pad left side
		for(i=strlen(LeftSide);i<3;i++)
		{
			LeftSide[strlen(LeftSide)]='0';
		}
		
		//add left side to SKU
		strcpy(FinalSKU,LeftSide);
		
		//pad right side
		RightLength=strlen(RightSide)-1;
		for(i=sizeof(RightSide)-1;i>-1;i--)
		{
			RightSide[i]=RightSide[RightLength-(sizeof(RightSide)-1-i)];
			
			if (RightSide[i]==0)
			{
				RightSide[i]='0';
			}
		}
		//null terminate right side
		RightSide[4]=0;

		//add right side
		strcat(FinalSKU,RightSide);
		//output
		strcpy(OutputArray,FinalSKU);
		return;
	}
	return;
}

void SKUDasher(char OutputArray[])
{
	char input[50];
	int j=0;
	int i;
	char LeftSide[3];
	char RightSide[4];
	char *depadRight;
	char FinalSKU[8];
	
	//get input
	printf("Enter a non dashed SKU\n");
	getc(stdin);
	fgets(input,sizeof(input),stdin);
	
	if (!((strlen(input)==8)||(strlen(input)==9)))
	{
		printf("The entered non dashed SKU is not valid");
		return;
	}

	else
	{
		for (i=0;i<strlen(input)-1;i++)
		{
			if (strchr("-0123456789",input[i])==NULL)
			{
				printf("Input SKU can only contain numbers");
				return;
			}
			if (input[i]=='-')
			{
				j++;
			}
		}
		
		if ((i==8)&&(j==1)&&(input[3]=='-'))
		{
			input[8]=0;
			strcpy(OutputArray,input);
			return;
		}
		
		else if ((j==0)&&(i==7))
		{
			strcpy(RightSide,(input+3));	//grab right side
			input[3]=0;						//grab and depad left side
			strcpy(LeftSide,input);
			
			for (i=strlen(LeftSide)-1;i>=1;i--)
			{
				if (LeftSide[strlen(LeftSide)-1]=='0')
				{
					LeftSide[strlen(LeftSide)-1]=0;
				}

			}
			strcpy(FinalSKU,LeftSide);			//add left to sku
			FinalSKU[strlen(FinalSKU)]='-';		//add - to dash skew
			
			depadRight=strtok(RightSide,"0");	//begin depad right side
			while(depadRight!=0)
			{
				if(depadRight[0]!='0')
				{
					break;
				}
				depadRight=strtok(NULL,"0");
			}
			if(depadRight[0]=='\n')		//In case user enters 4 zeros 
			{
				depadRight=depadRight-1;
			}			
			strcat(FinalSKU,depadRight);		//add right to SKU
			strcpy(OutputArray,FinalSKU);		//Output SKU
		}
		
		else
		{
			printf("Input SKU can contain only 7 numbers");
		}
	}
	return;
}
