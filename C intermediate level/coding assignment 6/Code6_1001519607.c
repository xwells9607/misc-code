// Xavier_Wells 1001519607
/* Coding Assignment 6 */

#include <stdio.h>
#include "MyLib.c"

int main(void)
{
	printf("1. Convert Decimal to Binary\n2. Sentence Cleanup\n3. SKU Converter\n4. SKU Dasher\n\n\nEnter menu choice\n");
	int choice;
	scanf("%d",&choice);
	
	char OutputArray[500];
	switch (choice)
	{
		case 1:
		{
			ConvertDecimalToBinary(OutputArray);
			printf("%s\n", OutputArray);
			break;
		}	
		case 2:
		{
			SentenceCleanup(OutputArray);
			printf("%s\n", OutputArray);
			break;
		}
		
		case 3:
		{
			SKUConverter(OutputArray);
			printf("%s\n", OutputArray);
			break;
		}

		case 4:
		{
			SKUDasher(OutputArray);
			printf("%s\n", OutputArray);
			break;
		}

		default:
		{
			printf("invalid input");
			break;
		}
	}
}
