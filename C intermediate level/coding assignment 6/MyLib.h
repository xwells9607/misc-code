// Xavier_Wells 1001519607
/* Coding Assignment 5 */
#ifndef MyLib
#define MyLib

/* prototypes */
static void ConvertDecimalToBinary (char OutputArray[]); 
static void SentenceCleanup(char OutputArray[]);
static void	SKUConverter(char OutputArray[]);
static void	SKUDasher(char OutputArray[]);
#endif
