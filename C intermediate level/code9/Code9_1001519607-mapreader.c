#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAPSIZE 10

char MapFilename[500]={};

void get_command_line_params (int argc, char *argv[])
{
	int i;
	for(i=1;i<argc;i++)
	{
		if (strncmp(argv[i], "MAP=",4)==0)
		{
			strcpy(MapFilename,*(argv+i)+4);
		}
	}
	
	if (MapFilename)
	{
		return;
	}
	else
	{
		printf("MapFilename must be given on the command line");
		exit(0);
	}
	
}

int main(int argc, char *argv[])
{
	FILE *TreasureMap;
	char CurrentChar;
	int j=0;
	int k=0;
	
	get_command_line_params(argc,argv);
	TreasureMap=fopen(MapFilename, "r");
	
	if(TreasureMap==NULL)
	{
		perror("TreasureMap did not open");
		exit(0);
	}
	

	for(j=0;j<MAPSIZE;j++)
	{
		for(k=0;k<MAPSIZE;k++)
		{
		CurrentChar=getc(TreasureMap);
		printf("%c  ",CurrentChar);
		}
		printf("\n\n");
	}
	fclose(TreasureMap);
	
	return(0);
}
