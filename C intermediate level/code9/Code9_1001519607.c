#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAPSIZE 10

char DirectionsFilename[500]={};
char MapFilename[500]={};

struct RowCol
{
	int Row;
	int Column;
};

void MoveNorth (struct RowCol * PositionPtr)
{
	if (((*PositionPtr).Row)==0)
	{
		return;
	}
	else
	{
		--((*PositionPtr).Row);
		return;
	}
}

void MoveSouth (struct RowCol * PositionPtr)
{
	if (((*PositionPtr).Row)==9)
	{
		return;
	}
	else
	{
		++((*PositionPtr).Row);
		return;
	}
}

void MoveEast (struct RowCol * PositionPtr)
{
	if (((*PositionPtr).Column)==9)
	{
		return;
	}
	else
	{
		++((*PositionPtr).Column);
		return;
	}
}

void MoveWest (struct RowCol * PositionPtr)
{
	if (((*PositionPtr).Column)==0)
	{
		return;
	}
	else
	{
		--((*PositionPtr).Column);
		return;
	}
}

void get_command_line_params (int argc, char *argv[])
{
	int i;
	for(i=1;i<argc;i++)
	{
		if (strncmp(argv[i], "DIRECTIONS=",11)==0)
		{
			strcpy(DirectionsFilename,*(argv+i)+11);
		}
		else if (strncmp(argv[i], "MAP=",4)==0)
		{
			strcpy(MapFilename,*(argv+i)+4);
		}
	}
	
	if (MapFilename&&DirectionsFilename)
	{
		return;
	}
	else
	{
		printf("DIRECTIONS= and MAP= must be given on the command line");
		exit(0);
	}
}
int main (int argc, char *argv[])
{
	FILE *DirectionsFile;
	FILE *TreasureMap;
	char DirectionsList[500];
	char Map[MAPSIZE][MAPSIZE];
	char buffer[2];
	int i,j,k;
	struct RowCol Position;
	struct RowCol *PositionPtr=&Position;
	
	get_command_line_params(argc,argv);
	DirectionsFile=fopen(DirectionsFilename, "r");
	TreasureMap=fopen(MapFilename, "w+");
	
	if(DirectionsFile==NULL)
	{
		perror("DirectionFile did not open");
		exit(0);
	}
	
	if(TreasureMap==NULL)
	{
		perror("TreasureMap did not open");
		exit(0);
	}
	
	fgets(DirectionsList,500,DirectionsFile);
	
	for(j=0;j<MAPSIZE;j++) 
	{
		for(k=0;k<MAPSIZE;k++) 
		{
			Map[j][k]='-';
		}
	}
	
	Position.Row=0;
	Position.Column=0;
	
	Map[Position.Row][Position.Column]='S';
	
	
	for(i=0;i<strlen(DirectionsList)-1;i++)
	{
		if (DirectionsList[i]=='N')
		{
			MoveNorth(PositionPtr);
			Map[Position.Row][Position.Column]='X';
		}
		
		else if (DirectionsList[i]=='E')
		{
			MoveEast(PositionPtr);
			Map[Position.Row][Position.Column]='X';
			
		}
		
		else if (DirectionsList[i]=='S')
		{
			MoveSouth(PositionPtr);
			Map[Position.Row][Position.Column]='X';
		}
		
		else
		{
			MoveWest(PositionPtr);
			Map[Position.Row][Position.Column]='X';
		}
		
	}
	
	Map[Position.Row][Position.Column]='E';
	
	char StringToBuffer[2]={};
	for(j=0;j<MAPSIZE;j++)
	{
		for(k=0;k<MAPSIZE;k++)
		{
			printf("%c  ",Map[j][k]);
			StringToBuffer[0]=Map[j][k];
			StringToBuffer[1]=0;
			sprintf(buffer, StringToBuffer);
			fputs(buffer,TreasureMap);
		}
		printf("\n\n");
	}
	fclose(TreasureMap);
	fclose(DirectionsFile);
	return(0);
}
