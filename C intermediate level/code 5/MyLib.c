// Xavier_Wells 1001519607
/* Coding Assignment 5 */

#include <stdio.h>
#include "MyLib.h"

void ConvertDecimalToBinary(char OutputArray[])
{
	int DecNum;
	int AskAgain = 1;
	int BitNumber[7];
	while (AskAgain)
	{
		printf("Please enter a decimal number between 0 and 255 ");
		scanf("%d", &DecNum);
	
		if (DecNum >= 0 && DecNum <= 255)
			AskAgain = 0;
		else
		{
			AskAgain = 1;
			printf("\nYou entered a number not between 0 and 255\n\n");
		}
	}

	int i;

	BitArray[7] = DecimalNum;
	
	/* right bitshift to divide by 2 */
	for (i = 6; i >= 0; i--)
	{
		BitArray[i] = BitArray[i+1] >> 1;
	}

	printf("\nDecimal %d converts to binary ", DecimalNum);
	
	/* use a bitmask of 1 to determine if odd or even */
	for (i = 0; i <= 7; i++)
	{
		BitArray[i] = (BitArray[i] & 1) ? 1 : 0;
		OutputArray[i]=(char)BitArray[i];
	}
	
	return;
}

