/* This is block comment syntax */

#include <stdio.h>
//This line will be in every code, it's the standard library much like java.io 

int aVariable;
//you can do this to make global variables

void goodbye (void)
//this is a function. In java we called them methods
{
	printf("goodbye\n");
}

int main (void)
//obviously main will be in every program as well
{
	
	int MyVariable;
	//You cannot use a dash in variables
	//can only declare a variable once
	//int 1MyVariable; wouldn't work
	//variables cannot start with a number
	
	printf("Hello World\n");
	//stock hell world print. not println, only printf
	
	return 0;
	//should always return, it's good style; after all main is an int
}
