//Xavier Wells,CSE 1325-004,Gieser,8/23/2018

#include <iostream>


int main() {
  
        std::cout << "Hello User\n";
	
	std::cout << "What is your name?\n";
	std::string userName;
	std::cin >> userName;
	std::cout << "Thank you for telling me your name " << userName << ".\n" << std::endl;
	
	std::cout << "Please enter two numbers\n";
	int first;
	int second;
	std::cin >> first >> second;
	
	int sum=first+second;
	int difference=first-second;
	int product=first*second;
	int quotient=first/second;
	
	std::cout << "The sum is: " << sum << "\nThe difference is: " << difference << "\nThe product is: " << product << "\nThe quotient is: " << quotient;
	
	std::cout << "\n\nPlease enter two more numbers\n";
	double third;
	double fourth;
	std::cin >> third >> fourth;
	
	double doubleSum=third+fourth;
	double doubleDifference=third-fourth;
	double doubleProduct=third*fourth;
	double doubleQuotient=third/fourth;
	
	std::cout << "The sum is: " << doubleSum << "\nThe difference is: " << doubleDifference << "\nThe product is: " << doubleProduct << "\nThe quotient is: " << doubleQuotient;
	
	std::cout << "\nThank you for your time " << userName << "." << std::endl; 

	return 0;
	
}
