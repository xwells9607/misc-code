#include "xaw9607_view.h"
#include <iomanip>
#include "xaw9607_dialog.h"
#include <sstream>
int View::get_menu()
{
	std::string menu = R"(
============================
CSE 1325 Auto Part Inventory
============================

1) View All Parts
2) Add Part(s)
3) Remove Part(s)
4) Load from file
5) Save to file
0) Exit

?)";

	//std::cout << menu;
	std::string result = Dialogs::input(menu, "Main Menu", "", "0");
	std::istringstream iss(result);
	int cmd;
	iss >> cmd;
	return cmd;
}

void View::exit_prompt()
{
	Dialogs::message("you exited the program", "Program terminated");
	//std::cout << "Terminating Program" << std::endl;
}

void View::invalid_option()
{
	Dialogs::message("Invalid option", "Please select a valid option");
	//std::cout << "Invalid option. Please selection a valid option." << std::endl;
}

void View::view_all_inventory()
{ 
	std::stringstream invout;
	invout << inventory;
	Dialogs::message(invout.str(), "Inventory");
}

void View::type_prompt()
{
	std::cout << "Input part type: ";
	
}

std::string View::name_prompt()
{
	//std::cout << "Input part name: ";
	std::string prompt = "Input part name: ";
	std::string result = Dialogs::input(prompt, "Part Name?", "", "0");
	std::istringstream iss(result);
	std::string val;
	iss >> val;
	return val;
}

int View::pn_prompt()
{
	//std::cout << "Input part number: ";
	std::string prompt = "Input part number: ";
	std::string result = Dialogs::input(prompt, "Part number?", "", "0");
	std::istringstream iss(result);
	int val;
	iss >> val;
	return val;
}

double View::price_prompt()
{
	//std::cout << "Input part price: ";
	std::string prompt = "Input part price: ";
	std::string result = Dialogs::input(prompt, "Price?", "", "0");
	std::istringstream iss(result);
	double val;
	iss >> val;
	return val;
}

int View::quantity_prompt()
{
	//std::cout << "How many of this part: ";
	std::string prompt = "How many of this part: ";
	std::string result = Dialogs::input(prompt, "Quantity?", "", "0");
	std::istringstream iss(result);
	int val;
	iss >> val;
	return val;
}

int View::new_old_prompt()
{
	//std::cout << "Is this a new or existing part?\n1)New\n2)Existing\n?" << std::endl;
	std::string prompt = "Is this a new or existing part?\n1)New\n2)Existing\n?";
	std::string result = Dialogs::input(prompt, "New or old?", "", "0");
	std::istringstream iss(result);
	int val;
	iss >> val;
	return val;
}

int View::select_part_type_prompt()
{
	//std::cout << "Select Part Type.\n1)Battery\n2)Engine\n3)Frame\n4)Tire\n5)Wheel\n6)Wheel Tire Combo\n7)Windshield Wiper\n?" << std::endl;
	std::string prompt = "Select Part Type.\n1)Battery\n2)Engine\n3)Frame\n4)Tire\n5)Wheel\n6)Wheel Tire Combo\n7)Windshield Wiper\n?";
	std::string result = Dialogs::input(prompt, "What Type?", "", "0");
	std::istringstream iss(result);
	int val;
	iss >> val;
	return val;
}

int View::int_field_prompt(std::string field)
{
	//std::cout << "Input " << field << std::endl;
	std::string prompt = "Input " + field;
	std::string result = Dialogs::input(prompt, field, "", "0");
	std::istringstream iss(result);
	int val;
	iss >> val;
	return val;
}

std::string View::string_field_prompt(std::string field)
{
	//std::cout << "Input " << field << std::endl;
	std::string prompt = "Input " + field;
	std::string result = Dialogs::input(prompt, field, "", "0");
	std::istringstream iss(result);
	std::string val;
	iss >> val;
	return val;
}
