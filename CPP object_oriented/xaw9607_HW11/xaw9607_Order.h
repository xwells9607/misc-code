#include "xaw9607_Auto_Part.h"
#include <map>
#ifndef xaw9607_Order_h
#define xaw9607_Order_h
class Order
{
	private:
	
		std::string cust_name;
		
		std::string cust_addr;
		
		std::string billing_info;
		
		int order_number;
		
		std::map<Auto_Part*, int> parts;
		
		std::string status;
		
	public:
	
		Order(std::string, std::string, std::string, int, std::string);
		
		void set_status(std::string);
		
		void add_to_order(Auto_Part*, int);
		
		void delete_from_order(Auto_Part*, int);
		
		int get_amount(Auto_Part*);
		
		std::string check_status();
};

#endif