//Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser
#include "xaw9607_view.h"
#include "xaw9607_Order.h"
#include <fstream>
#include <string>
#include <sstream>
#ifndef xaw9607_controller_h
#define xaw9607_controller_h
class Controller
{
	public:
		Controller(Inventory& inv, View& v) : inventory(inv), view(v) {};
		void cli();
		void gui();
		void execute_cmd(int);
		void add_part();
		void remove_part();
		void load_file(std::string to_load);
		void save_file(std::string to_save);
		Order* get_new_order();
	private:
		Inventory& inventory;
		View& view;
};
#endif