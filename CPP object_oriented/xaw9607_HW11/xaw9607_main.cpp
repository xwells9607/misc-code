 //Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser
#include "xaw9607_main_window.h"
#include "xaw9607_dialog.h"
#include <iostream>
#include <gtkmm.h>


using namespace std;

int main(int argc, char* argv[])
{
	Inventory inventory;

	View view(inventory);
	
	Controller controller(inventory, view);
	Gtk::Main kit(argc, argv);
	auto app = Gtk::Application::create(argc, argv, "xaw9607");
  // Instance a Window
  Main_window win(&inventory, &view, &controller);

  // Set the window title
  win.set_title("Inventory management");

  //Show the window and returns when it is closed or hidden
  return app->run(win);
	
	//controller.gui();
	
	return 0;
}