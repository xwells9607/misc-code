#include "xaw9607_frame.h"
#include <map>
#include "xaw9607_Order.h"
#include <thread>
#include <mutex>
#include <vector>
#ifndef xaw9607_Inventory_h
#define xaw9607_Inventory_h
class Inventory
{
	public:
		Inventory() {};
		void add_part(Auto_Part*);
		void add_parts(Auto_Part*, int);
		int get_num_parts(Auto_Part*);
		void remove_part(Auto_Part*);
		void remove_parts(Auto_Part*, int);
		std::map<Auto_Part*, int> get_inventory();
		void add_parts(int, int);
		void remove_parts(int, int);
		friend std::ostream& operator<< (std::ostream&, const Inventory&);
		void process_order(Order*);
		void multithread_orders(std::vector<Order*>);
	private:
		std::map<Auto_Part*, int> inventory;
};
#endif