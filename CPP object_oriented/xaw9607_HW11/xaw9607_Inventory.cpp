//Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser
#include "xaw9607_Inventory.h"

void Inventory::add_part(Auto_Part* part)
{
	if(inventory.count(part) == 1)
		inventory.at(part) = inventory.at(part)+1;
	else
		inventory.insert(std::pair<Auto_Part*,int>(part, 1));
}

void Inventory::add_parts(Auto_Part* part, int num)
{
	if(inventory.count(part) == 1)
		inventory.at(part) = inventory.at(part)+num;
	else
		inventory.insert(std::pair<Auto_Part*,int>(part, num));
}

int Inventory::get_num_parts(Auto_Part* part)
{
	return inventory.at(part);
}

void Inventory::remove_part(Auto_Part* part)
{
	if(inventory.at(part) > 1)
		inventory.at(part) = inventory.at(part)-1;
	else
		inventory.erase(part);
}

void Inventory::remove_parts(Auto_Part* part, int num)
{
	if(inventory.at(part) > num)
		inventory.at(part) = inventory.at(part)-num;
	else
		inventory.erase(part);
}

std::ostream& operator<<(std::ostream& ost, const Inventory& inventory)
{
	if(inventory.inventory.size() == 0)
	{
		ost << "No parts in inventory" << std::endl;
		return ost;
	}
	
	for(auto x : inventory.inventory)
	{
		if((x.first)->get_type() == "tire")
		{
			Tire* tire = dynamic_cast<Tire*>(x.first);
			ost << *tire;
		}
		if((x.first)->get_type() == "wheel")
		{
			Wheel* wheel = dynamic_cast<Wheel*>(x.first);
			ost << *wheel;
		}
		if((x.first)->get_type() == "wtc")
		{
			Wheel_Tire_Combo* wtc = dynamic_cast<Wheel_Tire_Combo*>(x.first);
			ost << *wtc;
		}
		if((x.first)->get_type() == "battery")
		{
			Battery* battery = dynamic_cast<Battery*>(x.first);
			ost << *battery;
		}
		if((x.first)->get_type() == "windshield_wiper")
		{
			Windshield_Wiper* ww = dynamic_cast<Windshield_Wiper*>(x.first);
			ost << (*ww);
		}
		if((x.first)->get_type() == "frame")
		{
			Frame* frame = dynamic_cast<Frame*>(x.first);
			ost << *frame;
		}
		if((x.first)->get_type() == "engine")
		{
			Engine* engine = dynamic_cast<Engine*>(x.first);
			ost << *engine;
		}
		ost << " - " << inventory.inventory.at(x.first) << std::endl;
	}
}

std::map<Auto_Part*, int> Inventory::get_inventory()
{
	return inventory;
}

void Inventory::add_parts(int pn, int q)
{
	for(auto x : inventory)
	{
		if(x.first->get_part_number() == pn)
			inventory.at(x.first) += q;
	}
}

void Inventory::remove_parts(int pn, int q)
{
	for(auto x : inventory)
	{
		if(x.first->get_part_number() == pn)
		{
			if(inventory.at(x.first) > q)
				inventory.at(x.first) = inventory.at(x.first)-q;
	else
		inventory.erase(x.first);
		}
	}
}

void Inventory::process_order(Order* inprocessing)
{
	std::mutex mtx;
	mtx.lock();
	if( inprocessing->check_status().compare("fulfilled"))
	{
		for(auto x : inventory)
		{
			if (inprocessing->get_amount(x.first) > 0)
			{
				if ( inprocessing->get_amount(x.first) > inventory.at(x.first) )
				{
					inprocessing->set_status("rejected");
					return;
				}
				else
				{
					inventory.at(x.first) = inventory.at(x.first)-inprocessing->get_amount(x.first);
					inprocessing->set_status("fulfilled");
				}
				
			}
		}
	}
	mtx.unlock();
	return;
}

void Inventory::multithread_orders(std::vector<Order*> orders)
{
	for(int i=0; i<orders.size(); i++)
	{
		std::thread t1(&Inventory::process_order, this, orders.at(i));
		std::thread t2(&Inventory::process_order, this, orders.at(i+1));
		t1.join();
		t2.join();
	}
}
