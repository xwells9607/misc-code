#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <gtkmm.h>
#include "xaw9607_controller.h"

class Main_window : public Gtk::Window {
    public:
		Main_window(Inventory *inv, View *v, Controller *c);
        //Main_window();
        virtual ~Main_window();
		//virtual ~Main_window(Inventory& inv, View& v, Controller& c) : inventory(&inv), view(&v), controller(&c) {};
    protected: //callbacks
        void on_add_click();
		void on_rem_click();
		void on_inv_click();
        void on_quit_click();        
		void on_load_click();
		void on_save_click();
		void on_order_click();
		void on_load_orders_click();
    private:
        //void set_sticks();               // Update display, robot move
        //Nim *nim;
        //GUI widgets
        //Gtk::Label *sticks;              // Display of sticks on game board
        //Gtk::Label *msg;                      // Status message display
        //Gtk::ToolButton *button1;             // Button to select 1 stick
        //Gtk::Image *button1_on_image;         //   Image when active
        //Gtk::Image *button1_off_image;        //   Image when inactive
        //Gtk::ToolButton *button2;             // Button to select 2 sticks
        //Gtk::Image *button2_on_image;
        //Gtk::Image *button2_off_image;
        //Gtk::ToolButton *button3;             // Button to select 3 sticks
        //Gtk::Image *button3_on_image;
        //Gtk::Image *button3_off_image;
        //Gtk::ToggleToolButton *computer_player;  // Button to enable robot
		View *view;
		Inventory *inventory;
		Controller *controller;

		
		
};
#endif
