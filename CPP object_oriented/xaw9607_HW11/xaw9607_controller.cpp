//Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser
#include "xaw9607_controller.h"
#include <string>

void Controller::cli()
{
	int cmd = 0;
	do{
		view.get_menu();
		std::cin >> cmd;
		execute_cmd(cmd);
	}while(cmd != 0);
}

void Controller::gui()
{
	int cmd = -1;
	do{
	cmd = view.get_menu();
	//std::cin >> cmd;
	execute_cmd(cmd);
	}while(cmd > 0);
}

void Controller::execute_cmd(int cmd)
{
	switch(cmd)
	{
		case 1:
			view.view_all_inventory();
			break;
		case 2:
			add_part();
			break;
		case 3:
			remove_part();
			break;
		case 4:
			//load_file();	
			break;
		case 5:
			//save_file();
			break;
		case 0:
			view.exit_prompt();
			break;
		default:
			view.invalid_option();
			break;
	}
}

void Controller::add_part()
{

	int new_old;
	new_old = view.new_old_prompt();
	//std::cin >> new_old;
	if(new_old == 1)
	{
		int part_type;
		part_type = view.select_part_type_prompt();
		//std::cin >> part_type;
		std::string type;
		std::string name;
		int part_number;
		double price;
		int quantity;
		name = view.name_prompt();
		//std::cin >> name;
		part_number = view.pn_prompt();
		//std::cin >> part_number;
		price = view.price_prompt();
		//std::cin >> price;
		quantity = view.quantity_prompt();
		//std::cin >> quantity;
		if(part_type == 1)
		{
			type = "battery";
			int ca, cca, v, rcm;
			ca = view.int_field_prompt("cranking amps");
			//std::cin >> ca;
			cca = view.int_field_prompt("cold cranking amps");
			//std::cin >> cca;
			v = view.int_field_prompt("voltage");
			//std::cin >> v;
			rcm = view.int_field_prompt("reserve capacity minutes");
			//std::cin >> rcm;
			Auto_Part* part = new Battery(type, name, part_number, price, ca, cca, v, rcm);
			inventory.add_parts(part, quantity);
		}
		else if(part_type == 2)
		{
			type = "engine";
			int nc;
			std::string ft;
			nc = view.int_field_prompt("number of cylinders");
			//std::cin >> nc;
			ft = view.string_field_prompt("fuel type");
			//std::cin >> ft;
			Auto_Part* part = new Engine(type, name, part_number, price, nc, ft);
			inventory.add_parts(part, quantity);
		}
		else if(part_type == 3)
		{
			type = "frame";
			std::string ft;
			ft = view.string_field_prompt("frame type");
			//std::cin >> ft;
			Auto_Part* part = new Frame(type, name, part_number, price, ft);
			inventory.add_parts(part, quantity);
		}
		else if(part_type == 4)
		{
			type = "tire";
			int w, r, d;
			std::string tt, sr, lr;
			tt = view.string_field_prompt("tire type");
			//std::cin >> tt;
			w = view.int_field_prompt("tire width");
			//std::cin >> w;
			r = view.int_field_prompt("tire ratio");
			//std::cin >> r;
			d = view.int_field_prompt("tire diameter");
			//std::cin >> d;
			sr = view.string_field_prompt("speed rating");
			//std::cin >> sr;
			lr = view.string_field_prompt("load range");
			//std::cin >> lr;
			Auto_Part* part = new Tire(type, name, part_number, price, tt, w, r, d, sr, lr);
			inventory.add_parts(part, quantity);
		}
		else if(part_type == 5)
		{
			type = "wheel";
			int wd, ww;
			std::string cat, c, bp;
			cat = view.string_field_prompt("category");
			//std::cin >> cat;
			c = view.string_field_prompt("color");
			//std::cin >> c;
			wd = view.int_field_prompt("wheel diameter");
			//std::cin >> wd;
			ww = view.int_field_prompt("wheel width");
			//std::cin >> ww;
			bp = view.string_field_prompt("bolt pattern");
			//std::cin >> bp;
			Auto_Part* part = new Wheel(type, name, part_number, price, cat, c, wd, ww, bp);
			inventory.add_parts(part, quantity);
		}
		else if(part_type == 6)
		{
			type = "wtc";
			int wd, ww;
			std::string cat, c, bp;
			cat = view.string_field_prompt("category");
			//std::cin >> cat;
			c = view.string_field_prompt("color");
			//std::cin >> c;
			wd = view.int_field_prompt("wheel diameter");
			//std::cin >> wd;
			ww = view.int_field_prompt("wheel width");
			//std::cin >> ww;
			bp = view.string_field_prompt("bolt pattern");
			//std::cin >> bp;
			int w, r, d;
			std::string tt, sr, lr;
			tt = view.string_field_prompt("tire type");
			//std::cin >> tt;
			w = view.int_field_prompt("tire width");
			//std::cin >> w;
			r = view.int_field_prompt("tire ratio");
			//std::cin >> r;
			d = view.int_field_prompt("tire diameter");
			//std::cin >> d;
			sr = view.string_field_prompt("speed rating");
			//std::cin >> sr;
			lr = view.string_field_prompt("load range");
			//std::cin >> lr;
			Auto_Part* part = new Wheel_Tire_Combo(type, name, part_number, price, cat, c, wd, ww, bp, tt, w, r, d, sr, lr);
			inventory.add_parts(part, quantity);
		}
		else
		{
			type = "windshield_wiper";
			int l;
			std::string ft;
			l = view.int_field_prompt("length");
			//std::cin >> l;
			ft = view.string_field_prompt("frame type");
			//std::cin >> ft;
			Auto_Part* part = new Windshield_Wiper(type, name, part_number, price, l, ft);
			inventory.add_parts(part, quantity);
		}
	}
	else
	{
		view.view_all_inventory();
		int pn, quantity;
		pn = view.pn_prompt();
		quantity = view.quantity_prompt();
		//std::cin >> quantity;
		inventory.add_parts(pn,quantity);
	}
}

void Controller::remove_part()
{
	view.view_all_inventory();
	
	int part_number;
	int quantity;
	part_number = view.pn_prompt();
	//std::cin >> part_number;
	quantity = view.quantity_prompt();
	//std::cin >> quantity;
	
	inventory.remove_parts(part_number,quantity);
}

void Controller::load_file(std::string to_load)
{
	std::ifstream ifs(to_load);
	
	if(ifs.is_open())
	{
		std::string type;
		std::string name;
		int part_number;
		double price;
		int quantity;
		while(ifs >> type)
		{
			ifs >> name >> part_number >> price;
			if(type == "battery")
			{
				int ca, cca, v, rcm;
				ifs >> ca;
				ifs >> cca;
				ifs >> v;
				ifs >> rcm;
				ifs >> quantity;
				Auto_Part* part = new Battery(type, name, part_number, price, ca, cca, v, rcm);
				inventory.add_parts(part, quantity);
			}
			else if(type == "engine")
			{
				int nc;
				std::string ft;
				ifs >> nc >> ft;
				ifs >> quantity;
				Auto_Part* part = new Engine(type, name, part_number, price, nc, ft);
				inventory.add_parts(part, quantity);
			}
			else if(type == "frame")
			{
				std::string ft;
				ifs >> ft;
				ifs >> quantity;
				Auto_Part* part = new Frame(type, name, part_number, price, ft);
				inventory.add_parts(part, quantity);
			}
			else if(type == "tire")
			{
				std::string tt, sr, lr;
				int w, r, d;
				ifs >> tt >> w >> r >> d >> sr >> lr;
				ifs >> quantity;
				Auto_Part* part = new Tire(type, name, part_number, price, tt, w, r, d, sr, lr);
				inventory.add_parts(part, quantity);
			}
			else if(type == "wheel")
			{
				std::string cat, c, bp;
				int wd, ww;
				ifs >> cat >> c >> wd >> ww >> bp;
				ifs >> quantity;
				Auto_Part* part = new Wheel(type, name, part_number, price, cat, c, wd, ww, bp);
				inventory.add_parts(part, quantity);
			}
			else if(type == "wtc")
			{
				std::string cat, c, bp;
				int wd, ww;
				ifs >> cat >> c >> wd >> ww >> bp;
				std::string tt, sr, lr;
				int w, r, d;
				ifs >> tt >> w >> r >> d >> sr >> lr;
				ifs >> quantity;
				Auto_Part* part = new Wheel_Tire_Combo(type, name, part_number, price, cat, c, wd, ww, bp, tt, w, r, d, sr, lr);
				inventory.add_parts(part, quantity);
			}
			else 
			{
				int l;
				std::string ft;
				ifs >> l >> ft;
				ifs >> quantity;
				Auto_Part* part = new Windshield_Wiper(type, name, part_number, price, l, ft);
				inventory.add_parts(part, quantity);
			}
		}
		ifs.close();
	}
	else
		std::cout << "file did not open" << std::endl;
}

void Controller::save_file(std::string to_save)
{
	std::ofstream ofs(to_save, std::ofstream::trunc);
	
	for(auto x : inventory.get_inventory())
	{
		ofs << (x.first)->to_string() << " " << x.second << std::endl;
	}
	
	ofs.close();
}

Order* Controller::get_new_order()
{
	std::string name = view.string_field_prompt("name");
	std::string address = view.string_field_prompt("address");
	std::string bill_info = view.string_field_prompt("billing information");
	int order_num = std::stoi(view.string_field_prompt("order number"));
	
	Order *stupidOrder = new Order(name, address, bill_info, order_num, "not_processed");

	
	int amountOfParts = std::stoi(view.string_field_prompt("how many different parts would you would like to order."));
	
	for (int i=0;i<amountOfParts; i++)
	{
		Auto_Part* part;
		int part_type;
		part_type = view.select_part_type_prompt();
		std::string type;
		std::string name;
		int part_number;
		double price;
		int quantity;
		name = view.name_prompt();
		part_number = view.pn_prompt();
		price = view.price_prompt();
		quantity = view.quantity_prompt();
		if(part_type == 1)
		{
			type = "battery";
			int ca, cca, v, rcm;
			ca = view.int_field_prompt("cranking amps");
			cca = view.int_field_prompt("cold cranking amps");
			v = view.int_field_prompt("voltage");
			rcm = view.int_field_prompt("reserve capacity minutes");
			part = new Battery(type, name, part_number, price, ca, cca, v, rcm);
			stupidOrder->add_to_order(part, quantity);
		}
		else if(part_type == 2)
		{
			type = "engine";
			int nc;
			std::string ft;
			nc = view.int_field_prompt("number of cylinders");
			ft = view.string_field_prompt("fuel type");
			part = new Engine(type, name, part_number, price, nc, ft);
			stupidOrder->add_to_order(part, quantity);
		}
		else if(part_type == 3)
		{
			type = "frame";
			std::string ft;
			ft = view.string_field_prompt("frame type");
			part = new Frame(type, name, part_number, price, ft);
			stupidOrder->add_to_order(part, quantity);
		}
		else if(part_type == 4)
		{
			type = "tire";
			int w, r, d;
			std::string tt, sr, lr;
			tt = view.string_field_prompt("tire type");
			w = view.int_field_prompt("tire width");
			r = view.int_field_prompt("tire ratio");
			d = view.int_field_prompt("tire diameter");
			sr = view.string_field_prompt("speed rating");
			lr = view.string_field_prompt("load range");
			part = new Tire(type, name, part_number, price, tt, w, r, d, sr, lr);
			stupidOrder->add_to_order(part, quantity);
		}
		else if(part_type == 5)
		{
			type = "wheel";
			int wd, ww;
			std::string cat, c, bp;
			cat = view.string_field_prompt("category");
			c = view.string_field_prompt("color");
			wd = view.int_field_prompt("wheel diameter");
			ww = view.int_field_prompt("wheel width");
			bp = view.string_field_prompt("bolt pattern");
			part = new Wheel(type, name, part_number, price, cat, c, wd, ww, bp);
			stupidOrder->add_to_order(part, quantity);
		}
		else if(part_type == 6)
		{
			type = "wtc";
			int wd, ww;
			std::string cat, c, bp;
			cat = view.string_field_prompt("category");
			c = view.string_field_prompt("color");
			wd = view.int_field_prompt("wheel diameter");
			ww = view.int_field_prompt("wheel width");
			bp = view.string_field_prompt("bolt pattern");
			int w, r, d;
			std::string tt, sr, lr;
			tt = view.string_field_prompt("tire type");
			w = view.int_field_prompt("tire width");
			r = view.int_field_prompt("tire ratio");
			d = view.int_field_prompt("tire diameter");
			sr = view.string_field_prompt("speed rating");
			lr = view.string_field_prompt("load range");
			part = new Wheel_Tire_Combo(type, name, part_number, price, cat, c, wd, ww, bp, tt, w, r, d, sr, lr);
			stupidOrder->add_to_order(part, quantity);
		}
		else
		{
			type = "windshield_wiper";
			int l;
			std::string ft;
			l = view.int_field_prompt("length");
			//std::cin >> l;
			ft = view.string_field_prompt("frame type");
			//std::cin >> ft;
			part = new Windshield_Wiper(type, name, part_number, price, l, ft);
			stupidOrder->add_to_order(part, quantity);
		}
	}
	return stupidOrder;
}
		