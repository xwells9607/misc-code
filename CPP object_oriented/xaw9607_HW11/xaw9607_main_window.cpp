#include "xaw9607_main_window.h"
#include "xaw9607_dialog.h"
#include "xaw9607_Order.h"

Main_window::Main_window(Inventory *inv, View *v, Controller *c) {

 inventory=inv;
 view=v;
 controller=c;
    // /////////////////
    // G U I   S E T U P
    // /////////////////

    set_default_size(400, 200);

    // Put a vertical box container as the Window contents
    Gtk::Box *vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0));
    add(*vbox); //Window.add(*vbox);

    // ///////
    // M E N U
    Gtk::MenuBar *menubar = Gtk::manage(new Gtk::MenuBar());
    vbox->pack_start(*menubar, Gtk::PACK_SHRINK, 0);

//     F I L E
    Gtk::MenuItem *menuitem_file = Gtk::manage(new Gtk::MenuItem("_File", true));
    menubar->append(*menuitem_file);
    Gtk::Menu *filemenu = Gtk::manage(new Gtk::Menu());
    menuitem_file->set_submenu(*filemenu);


    //         Q U I T
    Gtk::MenuItem *menuitem_quit = Gtk::manage(new Gtk::MenuItem("_exit", true));
    menuitem_quit->signal_activate().connect(sigc::mem_fun(*this, &Main_window::on_quit_click));
    filemenu->append(*menuitem_quit);
	
	//         SAve
    Gtk::MenuItem *menuitem_save = Gtk::manage(new Gtk::MenuItem("Save to File", true));
    menuitem_save->signal_activate().connect(sigc::mem_fun(*this, &Main_window::on_save_click));
    filemenu->append(*menuitem_save);
	
	//         Load
    Gtk::MenuItem *menuitem_load = Gtk::manage(new Gtk::MenuItem("Load File", true));
    menuitem_load->signal_activate().connect(sigc::mem_fun(*this, &Main_window::on_load_click));
    filemenu->append(*menuitem_load);

//     PARTS
    Gtk::MenuItem *menuitem_parts = Gtk::manage(new Gtk::MenuItem("_parts", true));
    menubar->append(*menuitem_parts);
    Gtk::Menu *partsmenu = Gtk::manage(new Gtk::Menu());
    menuitem_parts->set_submenu(*partsmenu);

    //           Add Part
    Gtk::MenuItem *menuitem_add = Gtk::manage(new Gtk::MenuItem("Add Part", true));
    menuitem_add->signal_activate().connect(sigc::mem_fun(*this, 
        &Main_window::on_add_click));
    partsmenu->append(*menuitem_add);
	
	
	//           Remove Part
    Gtk::MenuItem *menuitem_rem = Gtk::manage(new Gtk::MenuItem("Remove Part", true));
    menuitem_rem->signal_activate().connect(sigc::mem_fun(*this, 
        &Main_window::on_rem_click));
    partsmenu->append(*menuitem_rem);
	
//     Orders
    Gtk::MenuItem *menuitem_orders = Gtk::manage(new Gtk::MenuItem("_orders", true));
    menubar->append(*menuitem_orders);
    Gtk::Menu *ordersmenu = Gtk::manage(new Gtk::Menu());
    menuitem_orders->set_submenu(*ordersmenu);

    //           New Order
    Gtk::MenuItem *menuitem_order = Gtk::manage(new Gtk::MenuItem("New Order", true));
    menuitem_order->signal_activate().connect(sigc::mem_fun(*this, 
        &Main_window::on_order_click));
    ordersmenu->append(*menuitem_order);
	
	
	//           Load Orders
    Gtk::MenuItem *menuitem_load_orders = Gtk::manage(new Gtk::MenuItem("Load Orders from file", true));
    menuitem_load_orders->signal_activate().connect(sigc::mem_fun(*this, 
        &Main_window::on_load_orders_click));
    ordersmenu->append(*menuitem_load_orders);
	
	
	//           View Inv
    Gtk::MenuItem *menuitem_inv = Gtk::manage(new Gtk::MenuItem("View Inventory", true));
    menuitem_inv->signal_activate().connect(sigc::mem_fun(*this, 
        &Main_window::on_inv_click));
    partsmenu->append(*menuitem_inv);
    
    // /////////////
    // T O O L B A R
    Gtk::Toolbar *toolbar = Gtk::manage(new Gtk::Toolbar);
    vbox->add(*toolbar);

    //     Q U I T
    Gtk::ToolButton *quit_button = Gtk::manage(new Gtk::ToolButton{Gtk::Stock::QUIT});
    quit_button->set_tooltip_markup("Exit application");
    quit_button->signal_clicked().connect(sigc::mem_fun(*this, 
        &Main_window::on_quit_click));
    Gtk::SeparatorToolItem *sep = Gtk::manage(new Gtk::SeparatorToolItem{});
    sep->set_expand(true);  // The expanding sep forces the Quit button to the right
    toolbar->append(*sep);
    toolbar->append(*quit_button);
    
    vbox->show_all();

    
}

Main_window::~Main_window()
{
}

// /////////////////
// C A L L B A C K S
// /////////////////


void Main_window::on_quit_click() {
    hide();
}


    void Main_window::on_add_click(){
	controller->add_part();
	}
	
	void Main_window::on_rem_click(){
		controller->remove_part();
	}
	void Main_window::on_inv_click(){
		view->view_all_inventory();
	}
	void Main_window::on_load_click(){
		std::string to_load = Dialogs::input("Load", "Enter the file to load", "", "0");
		controller->load_file(to_load);
	}
	void Main_window::on_save_click(){
		std::string to_save = Dialogs::input("Save", "Enter the file to save to", "", "0");
		controller->save_file(to_save);
	}
	
	void Main_window::on_order_click(){
		Order* newOrder = controller->get_new_order();
		inventory->process_order(newOrder);
		Dialogs::message(newOrder->check_status(), "status");
		
	}
	void Main_window::on_load_orders_click(){}
