#include "xaw9607_Order.h"

Order::Order(std::string name, std::string addr, std::string billing, int ordernum, std::string stat)
{
	status=stat;
	order_number=ordernum;
	billing_info=billing;
	cust_addr=addr;
	cust_name=name;
}
		
void Order::set_status(std::string newStatus)
{
	status = newStatus;
}

void Order::add_to_order(Auto_Part* part, int amount)
{
	
	if(parts.count(part) >= 1)
		parts.at(part) = parts.at(part)+amount;
	else
		parts.insert(std::pair<Auto_Part*,int>(part, amount));
}
		
void Order::delete_from_order(Auto_Part* part, int amount)
{
	if(parts.at(part) > amount)
		parts.at(part) = parts.at(part)-amount;
	else
		parts.erase(part);
}

int Order::get_amount(Auto_Part* part)
{
	if (parts.count(part) > 0)
		return (parts.at(part));
	else
		return 0;
}

std::string Order::check_status()
{
	return status;
}

