#include "xaw9607_Inventory.h"
#ifndef xaw9607_view_h
#define xaw9607_view_h
class View
{
	public:
		View(Inventory& inv) : inventory(inv) {};
		int get_menu();
		void exit_prompt();
		void invalid_option();
		void view_all_inventory();
		void type_prompt();
		std::string name_prompt();
		int pn_prompt();
		double price_prompt();
		int quantity_prompt();
		int new_old_prompt();
		int select_part_type_prompt();
		int int_field_prompt(std::string);
		std::string string_field_prompt(std::string);
	private:
		Inventory& inventory;
};
#endif