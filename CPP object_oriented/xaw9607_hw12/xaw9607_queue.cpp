#include "xaw9607_queue.h"

template <class T>
queue<T>::queue()
{
	size=0;
	max_size=100;
	begin=&elements[0];
}

template <class T>
int queue<T>::find_size() const
{
	return size;
}

template <class T>
void queue<T>::enqueue(T& something)
{
	if (size==100)
	{
		throw std::logic_error("no more room in queue!");
		return;
	}
	elements.push_back(something);
	size++;
}

template <class T>
T& queue<T>::dequeue()
{
	if(this->isEmpty())
	{
		throw std::logic_error("queue is empty!");
		T *retvalue=new T;
		return *retvalue;
	}
	T *retvalue=new T (elements[0]);
	for(int i=0;i<elements.size();i++)
	{
		elements[i]=elements[i+1];
	}
	elements.pop_back();
	size--;
	return *retvalue;
}		

template <class T>
T& queue<T>::peek()
{
	if(this->isEmpty())
	{
		throw std::logic_error("queue is empty!");
		T *retvalue=new T;
		return *retvalue;
	}
	return elements[0];
}

template <class T>
bool queue<T>::isEmpty()
{
	if (elements.empty())
	{
		return true;
	}
	return false;
}

template <typename T>
std::ostream& operator<<(std::ostream& ost, const queue<T>& in)
{
	int s = in.find_size();
	for(int i=0;i<s-1;i++)
	{
		ost << i+1 << ')' << in.elements[i] << ", ";
	}
	ost << '\n';
}