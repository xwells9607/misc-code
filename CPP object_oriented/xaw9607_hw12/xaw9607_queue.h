#include <vector>
#include <string>
#include <iostream>
#include <stdexcept>
#ifndef _XAW9607_QUEUE_H_
#define _XAW9607_QUEUE_H_
template <class T> class queue
{
	private:
	
		std::vector<T> elements;
		int size;
		int max_size;
		T* begin;
		
	public:
	
		queue();
		int find_size() const;
		void enqueue(T&);
		T& dequeue();
		T& peek();
		bool isEmpty();
		
		template <class F>
		friend std::ostream& operator<<(std::ostream&, const queue<F>&);
};

#endif