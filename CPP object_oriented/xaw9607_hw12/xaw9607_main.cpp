#include <iostream>
#include "xaw9607_queue.h"
#include "xaw9607_queue.cpp"
int main()
{
	queue<int> myq;
	queue<std::string> myq2;
	for(int i=0;i<40;i++)
	{
		myq.enqueue(i);
		std::string string1="string "+std::to_string(i);
		myq2.enqueue(string1);
	}
	std::cout << "************printing initial populated queues*************" << '\n';
	std::cout << myq << '\n' << myq2 << std::endl;
	
	
	for(int i=0;i<15;i++)
	{
		myq.dequeue();
		myq2.dequeue();
	}
	std::cout << "************printing queues with 15 removed elements******" << '\n';
	std::cout << myq << '\n' << myq2 << "\n\n";

	std::cout << "************peeking at queues with removed elements********" << '\n';
	std::cout << myq.peek() << '\n';
	std::cout << myq2.peek() << '\n';
}