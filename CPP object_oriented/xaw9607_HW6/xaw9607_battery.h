#include "xaw9607_windsheild_wiper.h"
class Battery: public Auto_Part
{
	private:
	
		int cranking_amps;
		int cold_cranking_amps;
		int voltage;
		int reserve_capacity_minutes;
		
	public:
	
		Battery();
		Battery(std::string, std::string, int, double, int, int, int, int);
		
		int get_cranking_amps() const;
		int get_cold_cranking_amps() const;
		int get_voltage() const;
		int get_reserve_capacity_minutes() const;
		
		void set_cranking_amps(int);
		void set_cold_cranking_amps(int);
		void set_voltage(int);
		void set_reserve_capacity_minutes(int);
		
		friend std::ostream& operator<<(std::ostream&, const Battery&);
		std::string to_string();
};
