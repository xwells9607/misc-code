#include "xaw9607_wheel.h"

class Wheel_Tire_Combo: public Tire, public Wheel
{
	public:
	Wheel_Tire_Combo();
	Wheel_Tire_Combo(std::string, std::string, int, double, std::string, int, int, int, std::string, std::string, std::string, std::string, int, int, std::string);
	
	friend std::ostream& operator<<(std::ostream&, const Wheel_Tire_Combo&);
	std::string to_string();
	
};