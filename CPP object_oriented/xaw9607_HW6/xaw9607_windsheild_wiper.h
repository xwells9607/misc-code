#include "xaw9607_tire.h"

class Windsheild_Wiper: public Auto_Part
{
	private:
	
		int length;
		std::string frame_type;
		
	public:
	
		Windsheild_Wiper();
		Windsheild_Wiper(std::string, std::string, int, double, int, std::string);
		
		int get_length() const;
		std::string get_frame_type() const;
		
		void set_length(int);
		void set_frame_type(std::string);
		
		friend std::ostream& operator<<(std::ostream&, const Windsheild_Wiper&);
		std::string to_string();
};
