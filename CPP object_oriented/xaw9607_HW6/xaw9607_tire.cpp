#include "xaw9607_tire.h"

Tire::Tire()
{
	type="not provided";
	name="not provided";
	part_number=0;
	price=0;
	
	tire_type = "Not Provided";
	width = 0;
	ratio = 0;
	diameter = 0;
	speed_rating = "Not Provided";
	load_range = "Not Provided";
}

Tire::Tire(std::string SomeType, std::string SomeName, int SomePartNumber, double SomePrice, std::string SomeTireType, int SomeWidth, int SomeRatio, int SomeDiameter, std::string SomeSpeedRating, std::string SomeLoadRange)
{
	type=SomeType;
	name=SomeName;
	part_number=SomePartNumber;
	price=SomePrice;
	
	tire_type=SomeTireType;
	width=SomeWidth;
	ratio=SomeRatio;
	diameter=SomeDiameter;
	speed_rating=SomeSpeedRating;
	load_range=SomeLoadRange;
}

std::string Tire::get_tire_type() const
{
	return tire_type;
}
		
int Tire::get_width() const
{
	return width;
}

int Tire::get_ratio() const
{
	return ratio;
}

int Tire::get_diameter() const
{
	return diameter;
}

std::string Tire::get_speed_rating() const
{
	return speed_rating;
}

std::string Tire::get_load_range() const
{
	return load_range;
}
		
void Tire::set_tire_type(std::string SomeTireType)
{
	tire_type = SomeTireType;
}

void Tire::set_width(int SomeWidth)
{
	width=SomeWidth;
}

void Tire::set_ratio(int SomeRatio)
{
	ratio=SomeRatio;
}

void Tire::set_diameter(int SomeDiameter)
{
	diameter=SomeDiameter;
}

void Tire::set_speed_rating(std::string SomeSpeedRating)
{
	speed_rating=SomeSpeedRating;
}

void Tire::set_load_range(std::string SomeLoadRange)
{
	load_range=SomeLoadRange;
}
	
std::ostream& operator<<(std::ostream& ost, const Tire& tire)
{
	ost << "Type: " << tire.type << ", Name: " << tire.name 
		<< ", Part Number: " << tire.part_number << ", Price: " << tire.price 
		<< ", Tire Type: " << tire.tire_type << ", Width: " << tire.width 
		<< ", Ratio: " << tire.ratio << ", Diameter: " << tire.diameter 
		<< ", Speed Rating: " << tire.speed_rating << ", Load Range: " << tire.load_range;
}
std::string Tire::to_string()
{
	std::stringstream new_string;
	std::string autopart_part;
	autopart_part=this->Auto_Part::to_string();
	new_string << autopart_part << ", Tire Type: " << this->tire_type << ", Width: " << this->width 
		<< ", Ratio: " << this->ratio << ", Diameter: " << this->diameter 
		<< ", Speed Rating: " << this->speed_rating << ", Load Range: " << this->load_range;
		
	return new_string.str();
}