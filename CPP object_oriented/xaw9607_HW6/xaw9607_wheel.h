#include "xaw9607_battery.h"

class Wheel: public virtual Auto_Part
{	
	protected:
	
		std::string category;
		std::string color;
		int diameter;
		int width;
		std::string bolt_pattern;
		
	public:
	
		Wheel();
		Wheel(std::string, std::string, int, double, std::string, std::string, int, int, std::string);
		
		std::string get_category() const;
		std::string get_color() const;
		int get_diameter() const;
		int get_width() const;
		std::string get_bolt_pattern() const;
		
		void set_category(std::string);
		void set_color(std::string);
		void set_diameter(int);
		void set_width(int);
		void set_bolt_pattern(std::string);
		
		friend std::ostream& operator<<(std::ostream&, const Wheel&);
		std::string to_string();
};