//Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser

#include <iostream>
#include <sstream>

class Auto_Part
{
	public:
		//constructors
		Auto_Part();
		Auto_Part(std::string, std::string, int, double);
		
		//get functions
		std::string get_type() const;
		std::string get_name() const;
		int get_part_number() const;
		double get_price() const;
		
		//set functions
		void set_type(std::string);
		void set_name(std::string);
		void set_part_number(int);
		void set_price(double);
		
		//friend
		friend std::ostream& operator<<(std::ostream&, const Auto_Part&);
		std::string to_string();
		
	protected:
		std::string type;
		std::string name;
		int part_number;
		double price;
};

bool operator<(Auto_Part, Auto_Part);
