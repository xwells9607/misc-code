#include "xaw9607_wheel.h"

Wheel::Wheel()
{
	type="not provided";
	name="not provided";
	part_number=0;
	price=0;
	
	category = "Not Provided";
	color = "Not Provided";
	width = 0;
	diameter = 0;
	bolt_pattern = "Not Provided";
}

Wheel::Wheel(std::string SomeType, std::string SomeName, int SomePartNumber, double SomePrice, std::string SomeCategory, std::string SomeColor, int SomeDiameter, int SomeWidth, std::string SomeBoltPattern)
{
	type=SomeType;
	name=SomeName;
	part_number=SomePartNumber;
	price=SomePrice;
	
	category=SomeCategory;
	color=SomeColor;
	width=SomeWidth;
	diameter=SomeDiameter;
	bolt_pattern=SomeBoltPattern;
}

std::string Wheel::get_category() const
{
	return category;
}

std::string Wheel::get_color() const
{
	return color;
}

int Wheel::get_diameter() const
{
	return diameter;
}

int Wheel::get_width() const
{
	return width;
}

std::string Wheel::get_bolt_pattern() const
{
	return bolt_pattern;
}

void Wheel::set_category(std::string SomeCategory)
{
	category=SomeCategory;
}

void Wheel::set_color(std::string SomeColor)
{
	color=SomeColor;
}

void Wheel::set_diameter(int SomeDiameter)
{
	diameter=SomeDiameter;
}

void Wheel::set_width(int SomeWidth)
{
	width=SomeWidth;
}

void Wheel::set_bolt_pattern(std::string SomeBoltPattern)
{
	bolt_pattern=SomeBoltPattern;
}

std::ostream& operator<<(std::ostream& ost, const Wheel& wheel)
{
	ost << "Type: " << wheel.type << ", Name: " << wheel.name 
		<< ", Part Number: " << wheel.part_number << ", Price: " << wheel.price
		<< ", Category: " << wheel.category << ", Color: " << wheel.color 
		<< ", Diameter: " << wheel.diameter << ", Width: " << wheel.width 
		<< ", Bolt Pattern: " << wheel.bolt_pattern;
}

std::string Wheel::to_string()
{
	std::stringstream new_string;
	std::string autopart_part;
	autopart_part=this->Auto_Part::to_string();
	new_string << autopart_part << ", Category: " << this->category << ", Color: " << this->color 
		<< ", Diameter: " << this->diameter << ", Width: " << this->width 
		<< ", Bolt Pattern: " << this->bolt_pattern;
		
	return new_string.str();
}

