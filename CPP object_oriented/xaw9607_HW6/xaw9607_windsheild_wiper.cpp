#include "xaw9607_windsheild_wiper.h"

Windsheild_Wiper::Windsheild_Wiper()
{
	type="not provided";
	name="not provided";
	part_number=0;
	price=0;
	
	length=0;
	frame_type="not_provided";
}

Windsheild_Wiper::Windsheild_Wiper(std::string SomeType, std::string SomeName, int SomePartNumber, double SomePrice, int SomeLength, std::string SomeFrameType)
{
	type=SomeType;
	name=SomeName;
	part_number=SomePartNumber;
	price=SomePrice;
	
	length=SomeLength;
	frame_type=SomeFrameType;
}

int Windsheild_Wiper::get_length() const
{
	return length;
}

std::string Windsheild_Wiper::get_frame_type() const
{
	return frame_type;
}

void Windsheild_Wiper::set_length(int SomeLength)
{
	length=SomeLength;
}

void Windsheild_Wiper::set_frame_type(std::string SomeFrameType)
{
	frame_type=SomeFrameType;
}

std::ostream& operator<<(std::ostream& ost, const Windsheild_Wiper& windsheild_wiper)
{
	ost << "Type: " << windsheild_wiper.type << ", Name: " << windsheild_wiper.name 
		<< ", Part Number: " << windsheild_wiper.part_number << ", Price: " << windsheild_wiper.price
		<< ", Length: " << windsheild_wiper.length 
		<< ", Frame Type: " << windsheild_wiper.frame_type;
}

std::string Windsheild_Wiper::to_string()
{
	std::stringstream new_string;
	std::string autopart_part;
	autopart_part=this->Auto_Part::to_string();
	new_string << autopart_part << ", Length: " << this->length 
		<< ", Frame Type: " << this->frame_type;
	
	return new_string.str();
}

