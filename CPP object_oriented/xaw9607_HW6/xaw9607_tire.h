#include "xaw9607_Auto_Part.h"

class Tire: public virtual Auto_Part
{
	protected:
	
		std::string tire_type;
		int width;
		int ratio;
		int diameter;
		std::string speed_rating;
		std::string load_range;
		
	public:
	
		Tire();
		Tire(std::string, std::string, int, double, std::string, int, int, int, std::string, std::string);
		
		std::string get_tire_type() const;
		int get_width() const;
		int get_ratio() const;
		int get_diameter() const;
		std::string get_speed_rating() const;
		std::string get_load_range() const;
		
		void set_tire_type(std::string);
		void set_width(int);
		void set_ratio(int);
		void set_diameter(int);
		void set_speed_rating(std::string);
		void set_load_range(std::string);
		
		friend std::ostream& operator<<(std::ostream&, const Tire&);
		std::string to_string();
};
		