#include "xaw9607_battery.h"

Battery::Battery()
{
	type="not provided";
	name="not provided";
	part_number=0;
	price=0;
	
	cranking_amps=0;
	cold_cranking_amps=0;
	voltage=0;
	reserve_capacity_minutes=0;
}

Battery::Battery(std::string SomeType, std::string SomeName, int SomePartNumber, double SomePrice, int SomeCA, int SomeCCA, int SomeVoltage, int SomeRCM)
{
	type=SomeType;
	name=SomeName;
	part_number=SomePartNumber;
	price=SomePrice;
	
	cranking_amps=SomeCA;
	cold_cranking_amps=SomeCCA;
	voltage=SomeVoltage;
	reserve_capacity_minutes=SomeRCM;
}

int Battery::get_cranking_amps() const
{
	return cranking_amps;
}

int Battery::get_cold_cranking_amps() const
{
	return cold_cranking_amps;
}

int Battery::get_voltage() const
{
	return voltage;
}

int Battery::get_reserve_capacity_minutes() const
{
	return reserve_capacity_minutes;
}

void Battery::set_cranking_amps(int SomeCA)
{
	cranking_amps=SomeCA;
}

void Battery::set_cold_cranking_amps(int SomeCCA)
{
	cold_cranking_amps=SomeCCA;
}

void Battery::set_voltage(int SomeVoltage)
{
	voltage=SomeVoltage;
}

void Battery::set_reserve_capacity_minutes(int SomeRCM)
{
	reserve_capacity_minutes=SomeRCM;
}

std::ostream& operator<<(std::ostream& ost, const Battery& battery)
{
	ost << "Type: " << battery.type << ", Name: " << battery.name 
		<< ", Part Number: " << battery.part_number << ", Price: " << battery.price
		<< ", Cranking Amps: " << battery.cranking_amps 
		<< ", Cold Cranking Amps: " << battery.cold_cranking_amps 
		<< ", Voltage: " << battery.voltage 
		<< ", Reserve Capacity Minutes: " << battery.reserve_capacity_minutes;
}
std::string Battery::to_string()
{
	std::stringstream new_string;
	std::string autopart_part;
	autopart_part=this->Auto_Part::to_string();
	new_string << autopart_part << ", Cranking Amps: " << this->cranking_amps 
		<< ", Cold Cranking Amps: " << this->cold_cranking_amps 
		<< ", Voltage: " << this->voltage 
		<< ", Reserve Capacity Minutes: " << this->reserve_capacity_minutes;
	
	return new_string.str();
}

