#include "xaw9607_wheel_tire_combo.h"

Wheel_Tire_Combo::Wheel_Tire_Combo()
{
	type="not provided";
	name="not provided";
	part_number=0;
	price=0;
	//wheel stuff
	category = "Not Provided";
	color = "Not Provided";
	Wheel::width = 0;
	Wheel::diameter = 0;
	bolt_pattern = "Not Provided";
	//tire stuff
	tire_type = "Not Provided";
	Tire::width = 0;
	ratio = 0;
	Tire::diameter = 0;
	speed_rating = "Not Provided";
	load_range = "Not Provided";
}
	
Wheel_Tire_Combo::Wheel_Tire_Combo(std::string SomeType, std::string SomeName, int SomePartNumber, double SomePrice, std::string SomeTireType, int SomeTireWidth, int SomeRatio, int SomeTireDiameter, std::string SomeCategory, std::string SomeSpeedRating, std::string SomeLoadRange, std::string SomeColor, int SomeWheelDiameter, int SomeWheelWidth, std::string SomeBoltPattern)
{
	type=SomeType;
	name=SomeName;
	part_number=SomePartNumber;
	price=SomePrice;
	
	tire_type=SomeTireType;
	Tire::width=SomeTireWidth;
	ratio=SomeRatio;
	Tire::diameter=SomeTireDiameter;
	speed_rating=SomeSpeedRating;
	load_range=SomeLoadRange;
	
	category=SomeCategory;
	color=SomeColor;
	Wheel::width=SomeWheelWidth;
	Wheel::diameter=SomeWheelDiameter;
	bolt_pattern=SomeBoltPattern;
}
std::ostream& operator<<(std::ostream& ost, const Wheel_Tire_Combo& combo)
{
	ost << "Type: " << combo.type << ", Name: " << combo.name 
		<< ", Part Number: " << combo.part_number << ", Price: " << combo.price<< ", Tire Type: " << combo.tire_type << ", Width: " << combo.Tire::width 
		<< ", Ratio: " << combo.ratio << ", Diameter: " << combo.Tire::diameter 
		<< ", Speed Rating: " << combo.speed_rating << ", Load Range: " << combo.load_range 
		<< ", Category: " << combo.category << ", Color: " << combo.color 
		<< ", Diameter: " << combo.Wheel::diameter << ", Width: " << combo.Wheel::width 
		<< ", Bolt Pattern: " << combo.bolt_pattern;
}

std::string Wheel_Tire_Combo::to_string()
{
	std::stringstream new_string;
	std::string autopart_part;
	autopart_part=this->Auto_Part::to_string();
	new_string << autopart_part << ", Tire Type: " << this->tire_type << ", Width: " << this->Tire::width 
		<< ", Ratio: " << this->ratio << ", Diameter: " << this->Tire::diameter 
		<< ", Speed Rating: " << this->speed_rating << ", Load Range: " << this->load_range 
		<< ", Category: " << this->category << ", Color: " << this->color 
		<< ", Diameter: " << this->Wheel::diameter << ", Width: " << this->Wheel::width 
		<< ", Bolt Pattern: " << this->bolt_pattern;
		
	return new_string.str();
}