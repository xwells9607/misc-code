 //Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser
#include "xaw9607_wheel_tire_combo.h"
#include <iostream>
int main()
{
	Auto_Part newPart("Brakes", "Stoptech street", 8516, 40.65);
	std::string myString=newPart.to_string();
	
	Tire newTire("New Tire", "Michelin", 7899, 85.66, "Touring", 7, 5, 16, "D", "NON hd");
	std::string myString2=newTire.to_string();
	
	Windsheild_Wiper newWindsheildWiper("Wiper Blade", "Rain X", 8546, 12.85, 15, "L-type");
	std::string myString3=newWindsheildWiper.to_string();
	
	Battery newBattery("Battery", "Duralast", 25892, 100.99, 300, 800, 12, 1000);
	std::string myString4=newBattery.to_string();
	
	Wheel newWheel("Wheel", "Enkei", 102058, 500, "Super Sport", "Stainless", 10, 19, "8x100");
	std::string myString5=newWheel.to_string();
	
	Wheel_Tire_Combo newCombo("Wheel and Tire combo", "Motivo and niche", 128600, 1500.99, 
		"Motivo Nitto", 9, 2, 19, 
		"Sport", "Z", "HD", "Black", 19, 9, "5x112.5");
	std::string myString6=newCombo.to_string();
	/*
	std::cout << '\n' << newPart << '\n'; 
	std::cout << newTire << '\n' ;
	std::cout << newWindsheildWiper << '\n'; 
	std::cout << newBattery << '\n';
	std::cout << newWheel << '\n';
	std::cout << newCombo << '\n' << '\n';
	*/
	
	std::cout << myString << '\n' << myString2 << '\n' << myString3 << '\n' << myString4 << '\n' << myString5 << '\n' << myString6 << '\n';
	return 0;
}