 //Xavier Wells; ID# 1001519607; netid xaw9607; CSE1325-004 Gieser
 #include "xaw9607_Auto_Part.h"
 
//constructors
Auto_Part::Auto_Part()
{
	type="not provided";
	name="not provided";
	part_number=0;
	price=0;
}

Auto_Part::Auto_Part(std::string SomeType, std::string SomeName, int SomePartNumber, double SomePrice)
{
	type=SomeType;
	name=SomeName;
	part_number=SomePartNumber;
	price=SomePrice;
}


	
//get functions
std::string Auto_Part::get_type() const
{
	return type;
}

std::string Auto_Part::get_name() const
{
	return name;
}

int Auto_Part::get_part_number() const
{
	return part_number;
}

double Auto_Part::get_price() const
{
	return price;
}

		
//set functions
void Auto_Part::set_type(std::string NewType)
{
	type=NewType;
}

void Auto_Part::set_name(std::string NewName)
{
	name=NewName;
}

void Auto_Part::set_part_number(int NewPartNumber)
{
	part_number=NewPartNumber;
}

void Auto_Part::set_price(double NewPrice)
{
	price=NewPrice;
}
		
		
//friend
std::ostream& operator<<(std::ostream& ost, const Auto_Part& autopart)
{
	ost << "Type: " << autopart.type << ", Name: " << autopart.name 
		<< ", Part Number: " << autopart.part_number << ", Price: " << autopart.price;
}

bool operator<(Auto_Part rhs, Auto_Part lhs)
{
	return lhs.get_part_number() < rhs.get_part_number();
}

std::string Auto_Part::to_string()
{
	std::stringstream new_string;
	
	new_string << "Type: " << this->type << ", Name: " << this->name 
		<< ", Part Number: " << this->part_number << ", Price: " << this->price;
		
	return new_string.str();
}

