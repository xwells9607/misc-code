//Xavier Wells, CSE1325

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <cctype>
#include <map>

using namespace std;

int main(int argc, char *argv[])
{
	ifstream wordsFile; 
	wordsFile.open(argv[1]); //opens the file containing words
	string tempword;
	string tempstring;
	vector<string> words;
	vector<string> numbers;
	char ch;
	int i=0;
	int j=0;
	int k=0;
	
	while (getline(wordsFile,tempword)) //puts each word as a string in vector
	{
		words.push_back(tempword);
		cout << tempword << endl;
	}
	
	cout << endl;
	
	for (i=0;i<words.size();i++) //iterates through vector
	{
		for (j=0;j<words[i].length()-1;j++) //iterates through each string char by char
		{
			ch=words[i].at(j);
			if (isdigit(ch))
			{
				tempstring=ch;
				numbers.push_back(tempstring);
				cout << tempstring << endl; //writes found digit as char in numbers vector
			}
		}
	}
	
	cout << endl;
	
	ifstream mapFile (argv[2]); //opens the file containing map	
	map<string,string>codeKey;
	string key;
	string value;
	
	while (!mapFile.eof())
	{
		mapFile>> key;
		mapFile>> value;
		cout << key << " " << value << "\n";
		codeKey.insert ( pair<string,string>(key,value) );
	}
	cout << endl;

	
	stringstream buffer;
	for(i=0;i<numbers.size();i++)
	{
		if(codeKey.find(numbers.at(i)) != codeKey.end())
		{
			cout << codeKey.find(numbers.at(i))->second;
			buffer << codeKey.find(numbers.at(i))->second;
		}
	}
	cout << endl;
	
	ofstream outputword ("xaw9607_output.txt", ios_base::app);
	outputword << buffer.str() << "\n";
}
