% name: Xavier Wells 
% class: engr 1300-002
% date : 10/18/2017

%{
problem statement: 
    Find surface tension of a faling drop given Specific
    Gravity and Radius (in inches)

variables:
    r-radius [in]
    sg-specific gravity [-]
    dliquid-density [kg/m^3]
    g-gravity [m/s^2]
    st-surface tension [j/m^2]
%}

clc
clear

%inputs
sg=0.79;
r=0.25;

%set
g=9.8; %gravity of earth is 9.8 [m/s^2]

%convert
dliquid=1000.*sg; %SG [-] --> d [kg/m^3]
r=r.*.0254; %r [in] --> r [m]

%calculate
st = ((r^2)*(dliquid).*g);

fprintf ("surface tension=\n"+st+"(joules/meteraz^2)\n");



