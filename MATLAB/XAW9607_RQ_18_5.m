%{ 
Name: Xavier Wells
Class: Engr 1300-002
Date: 12/04/2017
%}
close all; clc; clear;
%Problem Statement: Find the length of one side of a gold cube given mass
%and specific gravity

%set

SG_Gold=19.3; %The specific gravity [-] of gold

Rho_Water=1000; %density in kg/m^3

%input mass in kg, checking for valid input

Mass_Gold=input('Enter the mass of the cube [kilograms]: ');

if (Mass_Gold<0)
    
    error('Error: Mass must be greater than zero grams.');

else
    
    Rho_Gold=SG_Gold.*Rho_Water; %Solve for density of Gold [kg/m^3]
    
    Volume_Gold=Mass_Gold./Rho_Gold; %Solve for Volume of Gold Sample [m^3]
    
    %Gold is in shape of cube, thus volume = length^3
    
    Length_Gold_m=nthroot(Volume_Gold,3); %Side Length [m]
    
    %conversions
    
    Length_Gold_cm=Length_Gold_m.*100; %side length [cm]
    
    Length_Gold_in=Length_Gold_cm./2.54; %Side lenth [in]
    
    %print result
    
    fprintf('The length of one side of the cube is %0.2f inches.\n', Length_Gold_in);
    
end