%{ 
Name: Xavier Wells
Class: Engr 1300-002
Date: 11/2/2017
%}

clear; close all; clc;

%Problem Statement: Tell a user the raange needed for a letter grade based
%off letter input

%User input
Grades=['A', 'B', 'C', 'D', 'F'];

User_In=menu('Select the grade earned', Grades(1), Grades(2), Grades(3), Grades(4), Grades(5));


if (User_In==1)
    
    fprintf('If you earn the letter grade %c, your numeric grade is in the range: 90 <= grade', Grades(User_In));
    
elseif (User_In==2)
    
    fprintf('If you earn the letter grade %c, your numeric grade is in the range: 80 <= grade < 90', Grades(User_In));

elseif (User_In==3)
    
    fprintf('If you earn the letter grade %c, your numeric grade is in the range: 70 <= grade < 80', Grades(User_In));

elseif (User_In==4)
    
    fprintf('If you earn the letter grade %c, your numeric grade is in the range: 60 <= grade < 70', Grades(User_In));

elseif (User_In==5)
    
    fprintf('If you earn the letter grade %c, your numeric grade is in the range: grade < 60', Grades(User_In));

else
    
    fprintf('No grade chosen');
    
end

fprintf('\n');