clc; clear; close all;

Age = input('Enter Age: ');

if (Age<15)
    
    error("Age is too low, please use different method of diagnosis");
    
elseif (Age>24)
    
    error("Age is too high, please use different method of diagnosis");
    
else
    
    sPressure = input('Enter systolic blood pressure [mmHg]: ');
    
    dPressure = input('Enter diastolic blood Pressure [mmHg]: ');
    
    if ((Age<=19&&(sPressure>120||dPressure>81))||(sPressure>132||dPressure>83))
        
        fprintf("You have high blood pressure\n");
        
    else
        
        fprintf("You do not have high blood pressure\n")
        
    end
    
end

        
