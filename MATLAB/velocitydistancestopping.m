%{ 
Name: Xavier Wells
Class: Engr 1300-002
Date: 11/2/2017
%}

%{
Variables:
distance: array of velocity in mph to reaction distance in meters and
braking distance in meters
%}

clc;
clear;
close all;

%create and fill array

distance = [20,6,6; 30,9,14; 40,12,24; 50,15,38; 60,18,55; 70,21,75];

figure('color', 'w');

plot(distance(:,1),distance(:,2),'-',distance(:,1),distance(:,3),'--');
grid on;
hold on;
xlabel('velocity [V] (m/s)');
ylabel('distance [D] (m)');
title('Stopping and reaction distance of a car');
legend('reaction distance', 'braking distance', 'location', 'northwest');
xlim([10,80]);
ylim([10,85]);

