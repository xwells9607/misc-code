%{ 
Name: Xavier Wells
Class: Engr 1300-002
Date: 11/2/2017
%}
clear; close all; clc

%Problem Statement: Determine Mach number and classification based on speed
%in meters per second

%set

Speed_Of_Sound=343; %Speed of sound is 343 meters per second

%input

User_In=input('Enter a speed in meters/second: ');

if (User_In<0)
    
    error('Error: Non-positive value')
    
else
    
    Mach=User_In/Speed_Of_Sound; %calculate Mach number
    
    if (Mach<1)
        
        Spec="Subsonic";
        
    elseif (Mach==1)
        
        Spec="Transonic";
        
    elseif (Mach<5&&Mach>1)
        
        Spec="Supersonic";
        
    else
        
        Spec="Hypersonic";
        
    end
    
    fprintf('%s, Mach number is %0.2f.\n', Spec, Mach);

end

