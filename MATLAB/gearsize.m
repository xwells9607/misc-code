%{ 
Name: Xavier Wells
Class: Engr 1300-002
Date: 11/2/2017
%}

%{
Variables:
gearstoyears: array of gear size in mm to years from 1967
%}

clc;
clear;
close all;

%create and fill array

gearstoyears = [0,5,7,16,25,31,37; 0.8,0.4,0.2,0.09,0.007,2E-04,8E-06];
%plot
figure('color', 'w');
plot(gearstoyears(1,:),gearstoyears(2,:),'-');
grid on;
hold on;
xlabel('years since 1967');
ylabel('size of gear (mm)');
title('Gear change over the years');
legend('change in gear size', 'location', 'northwest');
xlim([0,40]);
ylim([0,1]);