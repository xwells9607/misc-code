% xavier wells; 11/8/2017; xaw9607; 1001519607; engr 1300-002

clc; clear; close all;

Materials={'Ash';'Hickory';'Maple';'Pine'};

Cost=[4.35 4.95 6.35 3.75];

LECost=1.25;

BatChoice=menu('Please select a bat material', Materials);
Bat=Materials{BatChoice};
SellingPrice=input(['Please enter the selling price of one single bat of the material ' Bat ':\n']);
NoUpgrade=input('Please enter the total number of bats the manufacturer can produce in a week \nfollowed by the number of weeks the manufacturer plans to\nrun the bat production machinery in a year [amount weeks]:\n');
Upgrade=input('Please enter the additional number of bats the manufacturer can produce in a week if equipment is upgraded \nfollowed by the upgrades fixed cost[amount cost]:\n');
UpgradeCost=(Upgrade(2));
UpgradeTotBats=(Upgrade(1)+NoUpgrade(1)).*NoUpgrade(2);
TotCost=((Cost(BatChoice)./25)+LECost);
TotBatsNoUpgrade=(NoUpgrade(1).*NoUpgrade(2));
ProfitNoUpgrade=(TotBatsNoUpgrade*TotCost);
fprintf('Selling Price per bat:\t\t\t$%2.2f\nTotal Variable Cost per bat:\t$%2.2f\nNO UPGRADE\nProducing %0.0f %s bats a week for %0.0f weeks = %0.0f total bats\n\t Profit:\t\t\t$%2.2f\n', SellingPrice, TotCost, NoUpgrade(1), Bat, NoUpgrade(2), TotBatsNoUpgrade, ProfitNoUpgrade);
fprintf('\nWITH UPGRADE\nProducing %0.0f %s bats a week for %0.0f weeks = %0.0f total bats\n\tFixed Cost of upgrade:\t$%2.2f\n\tProfit:\t\t\t\t$%2.2f\n\tBreakeven Point:\t\t%2.0f weeks\n', Upgrade(1)+NoUpgrade(1), Bat, NoUpgrade(2), UpgradeTotBats, UpgradeCost, UpgradeCost-(UpgradeTotBats.*TotCost), UpgradeCost./TotCost./(NoUpgrade(1)+Upgrade(1)));
Plot('TotCost', 'TotBatsNoUpgrade', 'TotCost', 'UpgradeTotBats');