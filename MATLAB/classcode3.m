clc; clear; close all;
%{

SInfo={'John','Doe','Eng141','F13',[89,100,90,70]};

fprintf("Student Name:\t%s, %s\n", SInfo{2}, SInfo{1});

fprintf("Course-Term:\t%s - %s\n", SInfo{3}, SInfo{4});

fprintf('Max Grade:\t\t%0.0f\n', max(SInfo{5}));

fprintf("Average grade:\t%0.0f\n", mean(SInfo{5}));

%}

%{
Voltage=[18 30 40 45];

RectA=[5 18 24 30];
RectB=[15 26 34 50];

figure('color', 'w')
plot(Voltage, RectA, 'sk', Voltage, RectB, 'or', 'markersize', 16, 'markerfacecolor', 'b');
hold on;
plot(Voltage, RectB, 'or', 'markersize', 16, 'markerfacecolor', 'r');
xlabel('Voltage (V) [V]', 'color', 'red', 'fontweight', 'bold');
ylabel('Current (I) [mA]');
title('Tracking Current Across Two Thermoionic Rectifiers');
legend('Rectifier A', 'Rectifier B', 'Location', 'northwest');
axis([0 50 0 60]);
grid on;
%}

b_0=10; %initiail number is 10

k1=0.2; %growth per hour constant

k2=0.3;

%independent variable
t= [0:0.1:5]; %time in hours

%dependent variable
b1= b_0+exp(k1*t); %bacteria concentration

b2= b_0+exp(k2*t);

figure('color', 'w');
plot(t,b1,'-',t,b2,'--');
grid on;
xlabel('time (t) [s]');
ylabel('bacteria concentration #');
title('Bacteria Concentration of Two Different Bacteria After 5 Hour');
legend('bacteria #1', 'bacteria #2', 'location', 'best');
