% Xavier Wells, Engr 1300-002, 11/06/2017
clc
clear
close all

%{
Problem astatement: Create a program that will estimate the stopping
distance of a variety of brake pads
%}

%{
Variables:

Material: cell array with 
    column 1: pad material, 
    column 2: respective stopping force of pad material [MN/m^3], 
    column 3: respective brake area of pad material [cm^3]

Matchoice: numeric choice for material in column 1 of Material [int]

W: car weight (MF) [lbf]
M: car mass (M) [kg]

Vel: velocity of car (L/T)  [m/h]

D: Stopping distance (L) [ft]

KE_car: Kinetic energy of the car (E) [j]
%}

%Create cell area for materials
Material={'Non-metallic', 30, 150; 'Metallic', 50, 100; 'Ceramic', 40, 120};

%User choice
MatChoice=menu('Select the material of the brake pad coating:', Material{:, 1});

%User input
W=input('Enter the weight of the car [lbf]: ');
Vel=input('Enter the velocity of the car [mph]:\n');
D=input('Enter the maximum stopping distance\n[ft]: ');

%set
g_earth=9.8;

%convert Weight --> kg
%1 N=0.225 lbf
lbf_to_N=0.225;

W_N=W.*lbf_to_N;
M=W_N./g_earth;

%Convert Velocity --> m/s
%mph/3600/.621*1000=m/s
h_to_s=3600;
mile_to_Km=0.621;
Km_to_m=1000;

V_ms=((Vel./h_to_s)./mile_to_Km).*Km_to_m;

%Solve for Kinetic Energy (KE=(1/2)mass(velocity)^2
KEnergy=(1/2).*M.*(V_ms.^2);

% Solve for Stopping force
D_m=D./3.28;
FStop=KEnergy./D;

%solve for pad thickness in mm
Thickness=FStop./((Material{MatChoice, 2}.*10^6).*(Material{MatChoice, 3}./1000000));
Thickness=Thickness.*1000;

fprintf('For a car of weight %0.0f lbf and traveling at %0.2f mph in %0.2f ft, it would require the brake with an area of %0.2f square centimeters to be Metallic coated and have a thickness of %0.2f mm', W, Vel, D, Material{MatChoice, 2}, Material{MatChoice, 1}, Thickness); 

VarThick=[0:1:30];
    
%plot

plot(VarThick, (((Material{MatChoice, 2}.*10^6).*(Material{MatChoice, 3}./1000000)).*VarThick), '--', 'k' );
grid on;
xlabel('Thickness of coating (varthick) [mm]', 'size', 18);
ylabel('Stopping force (Varforce) [kN]', 'size', 18);



